using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImagePopUp : Singleton<ImagePopUp>
{
    public Image expandedImg;
    public void ShowImage(Sprite sprite)
    {
        if (sprite != null)
        {
            GetComponentInChildren<Photo>(true).gameObject.SetActive(true);
            expandedImg.sprite = sprite;
            expandedImg.preserveAspect = true;
        }

    }

    public void ClosePopUp()
    {
        GetComponentInChildren<Photo>().gameObject.SetActive(false);
    }
}
