using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotosCarousel : MonoBehaviour
{
    public List<Sprite> photos;
    public GameObject prefab;
    public Transform carouselParent;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in photos)
        {
            GameObject tempGO = Instantiate(prefab, carouselParent);
            tempGO.GetComponent<Image>().sprite = item;
            tempGO.GetComponent<Image>().preserveAspect = true;
            tempGO.GetComponent<Button>().onClick.AddListener(() =>
            {
                ImagePopUp.main.ShowImage(tempGO.GetComponent<Image>().sprite);
            });
        }
    }

}
